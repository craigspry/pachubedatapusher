#include <Arduino.h>
#include <HardwareSerial.h>
#include <ERxPachube.h>
#include <Ethernet.h>
#include <SPI.h>

byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED}; // make sure this is unique on your network

#define PACHUBE_API_KEY				"" // fill in your API key PACHUBE_API_KEY
#define PACHUBE_FEED_ID				999 // fill in your feed id

ERxPachubeDataOut dataout(PACHUBE_API_KEY, PACHUBE_FEED_ID);

void PrintDataStream(const ERxPachube& pachube);

void setup() 
{
  Ethernet.begin(mac);
  dataout.addData(0);
  dataout.addData(1);
  dataout.addData(2);
  delay(1000);  
}

void loop() 
{
  dataout.updateData(0, getTemperature(A0));
  int v = map(analogRead(A1), 0, 1023, 0, 5000);
  dataout.updateData(1, (int)v);
  dataout.updateData(2, (float)((getVoltage(A2)/330.0) * 1000.0));
  int status = dataout.updatePachube();
  delay(5000);
}

float getTemperature(int pin)
{
  return (getVoltage(pin) - 0.5) * 100.0; //converting from a 0 to 1024 digital range                                        // to 0 to 5 volts (each 1 reading equals ~ 5 millivolts
}

float getVoltage(int pin)
{
  return (analogRead(pin) * .004882814);
}

